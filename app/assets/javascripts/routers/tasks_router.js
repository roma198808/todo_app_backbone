TD.Routers.TasksRouter = Backbone.Router.extend({
  initialize: function ($rootEl, tasks) {
    this.$rootEl = $rootEl;
    this.tasks = tasks;
  },

  routes: {
    "": "index",
		"tasks/new": "new",
    "tasks/:id": "show"
  },

	index: function () {
    this.$rootEl.empty();
  },

	new: function () {
    var view = new TD.Views.NewTaskView({
			collection: this.tasks
		});
    this.$rootEl.html(view.render().$el);
  },

  show: function (id) {
    var task = this.tasks.get(id);
    var view = new TD.Views.TaskDetailView({ model: task });
		this.$rootEl.html(view.render().$el);
  }
});