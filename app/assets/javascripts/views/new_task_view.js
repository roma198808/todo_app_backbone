TD.Views.NewTaskView = Backbone.View.extend({
  events: {
    'click button': 'submit'
  },

  render: function () {
    var rendered = JST["tasks/new"]();
    this.$el.html(rendered);
    return this;
  },

	submit: function (event) {
    event.preventDefault();
    
		var formData = $('form').serializeJSON();
    this.collection.create(formData);
		
		Backbone.history.navigate("#/");
  }
});