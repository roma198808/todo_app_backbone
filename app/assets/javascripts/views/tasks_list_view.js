TD.Views.TasksListView = Backbone.View.extend({
  initialize: function () {
		this.listenTo(this.collection, "add change remove reset", this.render);
  },
	
	events: {
		"click .delete": "destroyTask"
	},
	
	destroyTask: function (event) {
    var $target = $(event.target);
    var post = this.collection.get($target.attr("data-id"));
    post.destroy({
    	success: function() {
    		Backbone.history.navigate("#/");
    	} 
    });
  },

  render: function () {
    var rendered = JST["tasks/list"]({ tasks: this.collection });

    this.$el.html(rendered);
    return this;
  }
});